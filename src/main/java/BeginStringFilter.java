public class BeginStringFilter implements Filter {

    private String source;

    public BeginStringFilter(String source) {
        this.source = source;
    }

    @Override
    public boolean apply(String target) {
        return source.startsWith(source);
    }
}

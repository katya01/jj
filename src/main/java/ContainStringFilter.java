public class ContainStringFilter implements Filter{
    private String source;

    public ContainStringFilter(String source) {
        this.source = source;
    }

    @Override
    public boolean apply(String target) {
        return source.indexOf(source)>=0;
    }
}

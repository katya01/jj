import java.util.Objects;

public class Packing {
    private String name;
    private int mass;

    public Packing (String name, int mass) {
        this.name = name;
        this.mass = mass;
    }

    public String getName() {
        return name;
    }

    public int getMass() {
        return mass;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Packing)) return false;
        Packing packing = (Packing) o;
        return getMass() == packing.getMass() &&
                Objects.equals(getName(), packing.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getMass());
    }

    @Override
    public String toString() {
        return "Packing{" +
                "name='" + name + '\'' +
                ", mass=" + mass +
                '}';
    }

}

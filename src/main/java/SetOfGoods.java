import java.util.ArrayList;
import java.util.List;

public class SetOfGoods implements PackingGoods {
    private List<PackingGoods> set =new ArrayList<>();
    private Packing packing;
    private String name;

//    public SetOfGoods(Packing packing, String name, PackingGoods... set) {
//
//        this.packing = packing;
//        this.name=name;
//        this.set=Arrays.asList(set);
//    }

    public SetOfGoods(Packing packing, String name, List<PackingGoods> set) {

        this.packing = packing;
        this.name=name;
        this.set.addAll(set);
    }
    @Override
    public int netWeight() {
        int sum=0;
        for(PackingGoods elem:set)
        {
            sum+=elem.netWeight();
        }
        return sum;
    }

    @Override
    public int grossMass()
    {
        int sum = 0;
        for (PackingGoods elem:set)
        {
            sum+=elem.grossMass();
        }
        return sum+packing.getMass();
    }

    @Override
    public boolean isSetOfGoods() {
        return true;
    }

    @Override
    public String getName() {
        return name;
    }

    public List<PackingGoods> getSet() {
        return set;
    }
}

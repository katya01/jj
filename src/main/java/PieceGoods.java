import java.util.Objects;

public class PieceGoods extends Goods {
    private int WeightOfOnePiece;

    public PieceGoods(String name, String description, int weightOfOnePiece) {
        super(name, description);
        this.WeightOfOnePiece = weightOfOnePiece;
    }

    public int getWeightOfOnePiece() {
        return WeightOfOnePiece;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PieceGoods)) return false;
        if (!super.equals(o)) return false;
        PieceGoods that = (PieceGoods) o;
        return Double.compare(that.getWeightOfOnePiece(), getWeightOfOnePiece()) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getWeightOfOnePiece());
    }

    @Override
    public String toString() {
        return "PieceGoods{" +
                "WeightOfOnePiece=" + WeightOfOnePiece +
                '}';
    }
}

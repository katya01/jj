public class EndingStringFilter implements Filter {
    private String source;

    public EndingStringFilter(String source) {
        this.source = source;
    }

    @Override
    public boolean apply(String target) {
        return source.endsWith(source);
    }
}

import java.util.List;

public class GoodsService {

//    private Shipment shipment;
//    private Filter filter;


    public GoodsService() {
    }

    int countByFilter(Shipment shipment, Filter filter) {
        int count = 0;
        List<PackingGoods> packingGoods = shipment.getSet();
        for (PackingGoods elem : packingGoods) {
            count += filter.apply(elem.getName()) ? 1 : 0;
        }
        return count;
    }

//    public static boolean isPackingSetGoodsOf(PackingGoods pack) {
//        return pack instanceof SetOfGoods;
//    }

//    public static int deep(List<PackingGoods> p, Filter filter) {
//        int count = 0;
//        for (PackingGoods k : p) {
//            if (isPackingSetGoodsOf(k)) {
//                List<PackingGoods> list = ((SetOfGoods) k).getSet();
//                count = deep(list, filter);
//            } else {
//
//                if (filter.apply(k.getName())) {
//                    count += 1;
//
//                }
//
//            }
//        }
//        return count;
//    }

//    public static int countByFilterDeep(Shipment shipment, Filter filter) {
//
//        return deep(shipment.getSet(), filter);
//
//    }


//    public static boolean deepCheck(List<PackingGoods> p) {
//        boolean z = true;
//        for (PackingGoods k : p) {
//            if (isPackingSetGoodsOf(k)) {
//                List<PackingGoods> list = ((SetOfGoods) k).getSet();
//                z = deepCheck(list);
//            } else {
//
//                if (!(k instanceof PackedWeightGoods)) {
//                    return false;
//
//                }
//
//            }
//        }
//        return z;
//    }

//    public static boolean checkAllWeighted(Shipment shipment) {
//
//        return deepCheck(shipment.getSet());
//
//    }
}


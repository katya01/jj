import java.util.Arrays;
import java.util.Objects;
//УРА

public class PackagedPieceGoods extends PieceGoods implements PackingGoods{
    private Packing packing;
    private PieceGoods[] array;
    private PackagedPieceGoods packagedPieceGoods;

    public PackagedPieceGoods(String name, String description, int weightOfOnePiece,
                              Packing packing, PieceGoods[] array) {
        super(name, description, weightOfOnePiece);
        this.packing = packing;
        this.array = array;
    }

    public PieceGoods[] getArray() {
        return array;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PackagedPieceGoods)) return false;
        if (!super.equals(o)) return false;
        PackagedPieceGoods that = (PackagedPieceGoods) o;
        return Objects.equals(packing, that.packing) &&
                Arrays.equals(getArray(), that.getArray());
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(super.hashCode(), packing);
        result = 31 * result + Arrays.hashCode(getArray());
        return result;
    }

    @Override
    public String toString() {
        return "PackagedPieceGoods{" +
                "packing=" + packing +
                ", array=" + Arrays.toString(array) +
                '}';
    }

    public int numberOfPiecesOfGoods()
    {
     return array.length;
    }

    public int netWeight()
    {
        int sum=0;
        for(int i=0;i<array.length; i++)
        {
            sum+=array[i].getWeightOfOnePiece();
        }
        return sum;
    }

    public int grossMass()
    {
        int mass=0;
        for(int i=0;i<array.length; i++){

            mass +=array[i].getWeightOfOnePiece();
        }
        return mass+packing.getMass();
    }

    @Override
    public boolean isSetOfGoods() {
        return false;
    }

}

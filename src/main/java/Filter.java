public interface Filter {
    boolean apply(String value);
}

public interface PackingGoods {
   int netWeight();//масса нетто
   int grossMass();//масса брутто
   boolean isSetOfGoods();
   String getName();
}

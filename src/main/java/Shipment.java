import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Shipment {
    private String description;
    private List<PackingGoods> set =new ArrayList<>();

//    Shipment(String description, PackingGoods... set) {
//        this.set = Arrays.asList(set);
//        this.description = description;
//    }
    Shipment(String description, List<PackingGoods> set) {
        this.set.addAll(set);
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Shipment)) return false;
        Shipment shipment = (Shipment) o;
        return Objects.equals(description, shipment.description) &&
                Objects.equals(set, shipment.set);
    }

    @Override
    public int hashCode() {
        return Objects.hash(description, set);
    }

    @Override
    public String toString() {
        return "Shipment{" +
                "description='" + description + '\'' +
                ", set=" + set +
                '}';
    }

    public String getDescription() {
        return description;
    }

    public List<PackingGoods> getSet() {
        return set;
    }

    public int mass() {
        int sum = 0;
        for (PackingGoods elem : set) {
            sum += elem.grossMass();
        }
        return sum;
    }

}

import java.util.Objects;

public class PackedWeightGoods extends WeightGoods implements PackingGoods {
    private int weightOfGoods;
    private Packing packing;

    public PackedWeightGoods(String name, String description, int weightOfGoods, Packing packing) {
        super(name, description);
        this.weightOfGoods = weightOfGoods;
        this.packing = packing;
    }

    public double getWeightOfGoods() {
        return weightOfGoods;
    }

    public Packing getPacking() {
        return packing;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PackedWeightGoods)) return false;
        if (!super.equals(o)) return false;
        PackedWeightGoods that = (PackedWeightGoods) o;
        return Double.compare(that.weightOfGoods, weightOfGoods) == 0 &&
                Objects.equals(packing, that.packing);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), weightOfGoods, packing);
    }

    @Override
    public String toString() {
        return "PackedWeightGoods{" +
                "weightOfGoods=" + weightOfGoods +
                ", packing=" + packing +
                '}';
    }

    public int netWeight()
    {
        return weightOfGoods;
    }

    public int grossMass()
    {
        return weightOfGoods+packing.getMass();
    }

    @Override
    public boolean isSetOfGoods() {
        return false;
    }

}

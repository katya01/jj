import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
//
//import static org.junit.Assert.assertEquals;

public class PackedWeightGoodsTest {
    private PackedWeightGoods packedWeightGoods;

    @Before
    public void setUp(){
        packedWeightGoods = new PackedWeightGoods("One","123", 12400,
                new Packing("up",100));
    }

    @Test
    public void netWeight() {
       assertEquals(12400,packedWeightGoods.netWeight());
    }

    @Test
    public void grossMass() {
        assertEquals(12500,packedWeightGoods.grossMass());
    }
}
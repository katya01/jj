import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ContainStringFilterTest {

    private ContainStringFilter containStringFilter;
    @Before
    public void setUp()
    {
        String one="Добрейший день 2";
        containStringFilter= new ContainStringFilter(one);
    }
    @Test
    public void apply() {
        assertEquals(true,containStringFilter.apply("день"));
    }
}
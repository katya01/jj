import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

public class ShipmentTest {
    private Shipment shipment;
    @Before
    public void setUp()
    {
        PieceGoods[] pieceGoods=new PieceGoods[3];
        pieceGoods[0]= new PieceGoods("ArOne","123",1200);
        pieceGoods[1]= new PieceGoods("ArTwo","123",1500);
        pieceGoods[2]= new PieceGoods("ArThree","123",900);

        PieceGoods[] pieceGoods1=new PieceGoods[2];
        pieceGoods1[0]= new PieceGoods("ArFour","123",1000);
        pieceGoods1[1]= new PieceGoods("ArFive","123",1700);
        List<PackingGoods> list = new ArrayList<PackingGoods>();
        list.add(new PackagedPieceGoods("Piece Goods One","123",0,
                new Packing("pac",20),pieceGoods));
        list.add(new PackedWeightGoods("Weight Goods One","123", 12400,
                new Packing("pack",10)));

        List<PackingGoods> list1= new ArrayList<PackingGoods>();
        list1.add(new PackagedPieceGoods("Piece Goods Two","123",0,
                new Packing("pac",50),pieceGoods1));
        list1.add(new PackedWeightGoods("Weight Goods Two","123", 15400,
                new Packing("pack",60)));
        list1.add(new SetOfGoods(new Packing("pac",100),"Second set of Goods",list));

        shipment=new Shipment("123", list1);

    }
    @Test
    public void mass() {
        assertEquals(34340,shipment.mass());
    }
}
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BeginStringFilterTest {

    private BeginStringFilter beginStringFilter;
    @Before
    public void setUp(){
        String one="Добрейший день 2";
        beginStringFilter= new BeginStringFilter(one);

    }
    @Test
    public void apply() {
        assertEquals(true,beginStringFilter.apply("Добрейший"));
    }
}
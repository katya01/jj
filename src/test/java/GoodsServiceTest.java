import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class GoodsServiceTest {
    private Shipment shipment;
    private Filter filter;
    private GoodsService goodsService;
    @Before
    public void setUp()
    {
        PieceGoods[] pieceGoods=new PieceGoods[3];
        pieceGoods[0]= new PieceGoods("zz Piece Goods","123",1200);
        pieceGoods[1]= new PieceGoods("Two Piece Goods","123",1500);
        pieceGoods[2]= new PieceGoods("Three Piece Goods","123",900);

        PieceGoods[] pieceGoods1=new PieceGoods[2];
        pieceGoods1[0]= new PieceGoods("Four Piece Goods","123",1000);
        pieceGoods1[1]= new PieceGoods("Five Piece Goods","123",1700);
        List<PackingGoods> list = new ArrayList<PackingGoods>();
        list.add(new PackagedPieceGoods("One Package Piece Goods","123",0,
                new Packing("pac",20),pieceGoods));
        list.add(new PackedWeightGoods("One Package Weight Goods ","123", 12400,
                new Packing("pack",10)));

        List<PackingGoods> list1= new ArrayList<PackingGoods>();
        list1.add(new SetOfGoods(new Packing("pac",100),"Second set of Goods",list));
        list1.add(new PackagedPieceGoods("Two Package Piece Goods ","123",0,
                new Packing("pac",50),pieceGoods1));
        list1.add(new PackedWeightGoods("One Package Wight Goods","123", 15400,
                new Packing("pack",60)));

        shipment=new Shipment("123", list1);

        filter=new BeginStringFilter("One");

        goodsService=new GoodsService();
    }

    @Test
    public void countByFilter() {
        assertEquals(3,goodsService.countByFilter(shipment,filter));

    }

//    @Test
//    public void countByFilterDeep() {
//        assertEquals(3,goodsService.countByFilterDeep(shipment,filter));
//    }
//
//    @Test
//    public void checkAllWeighted() {
//        assertEquals(false,goodsService.checkAllWeighted(shipment));
//    }
}
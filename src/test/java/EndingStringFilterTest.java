import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class EndingStringFilterTest {
    private EndingStringFilter endingStringFilter;
    @Before
    public void setUp(){
        String one="Добрейший день 2";
        endingStringFilter= new EndingStringFilter(one);

    }
    @Test
    public void apply() {
        assertEquals(true,endingStringFilter.apply("2"));
    }
}
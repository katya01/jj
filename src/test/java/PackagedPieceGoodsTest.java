import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class PackagedPieceGoodsTest {

    private PackagedPieceGoods packagedPieceGoods;
    private PieceGoods[] array;

    @Before
    public void setUp() {
        PieceGoods[] pieceGoods=new PieceGoods[3];
        pieceGoods[0]= new PieceGoods("ArOne","123",1200);
        pieceGoods[1]= new PieceGoods("ArTwo","123",1500);
        pieceGoods[2]= new PieceGoods("ArThree","123",900);

        packagedPieceGoods=new PackagedPieceGoods("One","123",0,
                new Packing("pac",200),pieceGoods);
    }


    @Test
    public void netWeight() {
        assertEquals(3600,packagedPieceGoods.netWeight());
    }

    @Test
    public void grossMass() {
        assertEquals(3800,packagedPieceGoods.grossMass());
    }
}